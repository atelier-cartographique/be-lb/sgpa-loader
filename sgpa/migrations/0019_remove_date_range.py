# Generated by Django 3.2.5 on 2024-03-28 15:00

import django.contrib.postgres.fields.ranges
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('sgpa', '0018_cached_tree'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='treedendrometryhistory',
            name='validity',
            field=django.contrib.postgres.fields.ranges.DateTimeRangeField(),
        ),
        migrations.RemoveField(
            model_name='treeenvironmenthistory',
            name='validity',
            field=django.contrib.postgres.fields.ranges.DateTimeRangeField(),
        ),
        migrations.RemoveField(
            model_name='treeepiphytehistory',
            name='validity',
            field=django.contrib.postgres.fields.ranges.DateTimeRangeField(),
        ),
        migrations.RemoveField(
            model_name='treeontogenesishistory',
            name='validity',
            field=django.contrib.postgres.fields.ranges.DateTimeRangeField(),
        ),
        migrations.RemoveField(
            model_name='treepathogenhistory',
            name='validity',
            field=django.contrib.postgres.fields.ranges.DateTimeRangeField(),
        ),
        migrations.RemoveField(
            model_name='treepesthistory',
            name='validity',
            field=django.contrib.postgres.fields.ranges.DateTimeRangeField(),
        ),
        migrations.RemoveField(
            model_name='treeriskanalysishistory',
            name='validity',
            field=django.contrib.postgres.fields.ranges.DateTimeRangeField(),
        ),
        migrations.RemoveField(
            model_name='treestatushistory',
            name='validity',
            field=django.contrib.postgres.fields.ranges.DateTimeRangeField(),
        ),
        migrations.RemoveField(
            model_name='treestructurehistory',
            name='validity',
            field=django.contrib.postgres.fields.ranges.DateTimeRangeField(),
        ),
        migrations.RemoveField(
            model_name='treesymptombranchhistory',
            name='validity',
            field=django.contrib.postgres.fields.ranges.DateTimeRangeField(),
        ),
        migrations.RemoveField(
            model_name='treesymptomcollarhistory',
            name='validity',
            field=django.contrib.postgres.fields.ranges.DateTimeRangeField(),
        ),
        migrations.RemoveField(
            model_name='treesymptomleavehistory',
            name='validity',
            field=django.contrib.postgres.fields.ranges.DateTimeRangeField(),
        ),
        migrations.RemoveField(
            model_name='treesymptomroothistory',
            name='validity',
            field=django.contrib.postgres.fields.ranges.DateTimeRangeField(),
        ),
        migrations.RemoveField(
            model_name='treesymptomtrunkhistory',
            name='validity',
            field=django.contrib.postgres.fields.ranges.DateTimeRangeField(),
        ),
        migrations.RemoveField(
            model_name='treevitalityhistory',
            name='validity',
            field=django.contrib.postgres.fields.ranges.DateTimeRangeField(),
        ),
    ]
