# Generated by Django 3.2.5 on 2024-03-28 15:00

import django.contrib.postgres.fields.ranges
from django.db import migrations, models
import django.db.models.deletion
from django.utils.datetime_safe import datetime
from psycopg2.extras import DateTimeTZRange

class Migration(migrations.Migration):

    dependencies = [
        ('sgpa', '0019_remove_date_range'),
    ]

    operations = [
        migrations.AlterField(
            model_name='selection',
            name='date_created',
            field=models.DateTimeField(auto_now_add=True),
        ),
        migrations.AlterField(
            model_name='selection',
            name='date_modified',
            field=models.DateTimeField(auto_now=True),
        ),
        migrations.AddField(
            model_name='treedendrometryhistory',
            name='validity',
            field=django.contrib.postgres.fields.ranges.DateTimeRangeField(default=DateTimeTZRange()),
        ),
        migrations.AddField(
            model_name='treeenvironmenthistory',
            name='validity',
            field=django.contrib.postgres.fields.ranges.DateTimeRangeField(default=DateTimeTZRange()),
        ),
        migrations.AddField(
            model_name='treeepiphytehistory',
            name='validity',
            field=django.contrib.postgres.fields.ranges.DateTimeRangeField(default=DateTimeTZRange()),
        ),
        migrations.AddField(
            model_name='treeontogenesishistory',
            name='validity',
            field=django.contrib.postgres.fields.ranges.DateTimeRangeField(default=DateTimeTZRange()),
        ),
        migrations.AddField(
            model_name='treepathogenhistory',
            name='validity',
            field=django.contrib.postgres.fields.ranges.DateTimeRangeField(default=DateTimeTZRange()),
        ),
        migrations.AddField(
            model_name='treepesthistory',
            name='validity',
            field=django.contrib.postgres.fields.ranges.DateTimeRangeField(default=DateTimeTZRange()),
        ),
        migrations.AddField(
            model_name='treeriskanalysishistory',
            name='validity',
            field=django.contrib.postgres.fields.ranges.DateTimeRangeField(default=DateTimeTZRange()),
        ),
        migrations.AddField(
            model_name='treestatushistory',
            name='validity',
            field=django.contrib.postgres.fields.ranges.DateTimeRangeField(default=DateTimeTZRange()),
        ),
        migrations.AddField(
            model_name='treestructurehistory',
            name='validity',
            field=django.contrib.postgres.fields.ranges.DateTimeRangeField(default=DateTimeTZRange()),
        ),
        migrations.AddField(
            model_name='treesymptombranchhistory',
            name='validity',
            field=django.contrib.postgres.fields.ranges.DateTimeRangeField(default=DateTimeTZRange()),
        ),
        migrations.AddField(
            model_name='treesymptomcollarhistory',
            name='validity',
            field=django.contrib.postgres.fields.ranges.DateTimeRangeField(default=DateTimeTZRange()),
        ),
        migrations.AddField(
            model_name='treesymptomleavehistory',
            name='validity',
            field=django.contrib.postgres.fields.ranges.DateTimeRangeField(default=DateTimeTZRange()),
        ),
        migrations.AddField(
            model_name='treesymptomroothistory',
            name='validity',
            field=django.contrib.postgres.fields.ranges.DateTimeRangeField(default=DateTimeTZRange()),
        ),
        migrations.AddField(
            model_name='treesymptomtrunkhistory',
            name='validity',
            field=django.contrib.postgres.fields.ranges.DateTimeRangeField(default=DateTimeTZRange()),
        ),
        migrations.AddField(
            model_name='treevitalityhistory',
            name='validity',
            field=django.contrib.postgres.fields.ranges.DateTimeRangeField(default=DateTimeTZRange()),
        ),
    ]
