from ..models import Selection
from .functions import serialize_user
from typing import Optional, Dict


def deserialize_selection(data: Dict) -> Optional[Selection]:
    if data.get("name"):
        return Selection(
            name=data.get("name"),
        )


def serialize_selection(s: Selection):
    return {
        "id": s.id,
        "name": s.name,
        "date_created": s.date_created,
        "date_modified": s.date_modified,
        "username": serialize_user(s.user),
        "parks": [p.trigramme for p in s.parks.all()],
        "trees": [t.id for t in s.trees.all()],
        "shortcut": s.shortcut
    }
