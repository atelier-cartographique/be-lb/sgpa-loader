from ..models import UserPreferences
from .functions import serialize_user
from typing import Optional, Dict

def serialize_user_preferences(up: UserPreferences):
    return {
        "id": up.id,
        "user": serialize_user(up.user),
        "parks": [p.trigramme for p in up.parks.all()],
    }