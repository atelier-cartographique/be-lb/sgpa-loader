from sgpa.models.image import Image
from sgpa.serializers.functions import serialize_datetime


def serialize_tree_image(image: Image):
    return {
        "id": image.id,
        "created_at": serialize_datetime(image.created_at),
        "user": image.user.id,
        "url": image.image.url,
        "name": image.image.name,
    }


# def deserialize_tree_image(tree_id, user: User, file: UploadedFile):
#     tree = Tree.objects.get(id=tree_id)
#     return Image.objects.create(image=file, tree=tree, user=user)
