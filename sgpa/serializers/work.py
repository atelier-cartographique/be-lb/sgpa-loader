from ..models import Tree, Recommendation, WorkTree, Permit
from .functions import serialize_nullable_obj


def serialize_permit(p: Permit):
    if p:
        return {
            "reference": p.reference,
        }
    else:
        return None


def serialize_recommendation(r: Recommendation):
    if r:
        return {
            "recommendation": r.recommendation.label,
            "date": r.date,
            "do_not_follow": r.do_not_follow,
            "delay": r.delay,
        }
    else:
        return None


def get_tree_recommendation(tree: Tree):
    qs = Recommendation.objects.filter(tree_id=tree.id)
    if qs.exists():
        return [serialize_recommendation(r) for r in qs.all()]
    else:
        return None


def serialize_tree_recommendation(tree: Tree):
    return {
        "tree_id": tree.id,
        "recommendations": get_tree_recommendation(tree),
    }


def serialize_work_tree(wt: WorkTree):
    return {
        "priority": wt.priority,
        "work_kind": serialize_nullable_obj(wt.work_kind),
        "due_date": wt.due_date,
        "target_date": wt.target_date,
        "done_date": wt.done_date,
        "state": wt.state,
        "permit": serialize_permit(wt.permit),
        "comment": wt.comment,
        "price": wt.price,
        "recommendation": serialize_recommendation(wt.recommendation),
        "tree_cutting_disposal": serialize_nullable_obj(wt.tree_cutting_disposal),
        "validation_comment": wt.validation_comment,
    }


def get_tree_work(tree: Tree):
    qs = WorkTree.objects.filter(tree_id=tree.id)
    if qs.exists():
        return [serialize_work_tree(wt) for wt in qs.all()]
    else:
        return None


def serialize_tree_work(tree: Tree):
    return {
        "tree_id": tree.id,
        "work": get_tree_work(tree),
    }
