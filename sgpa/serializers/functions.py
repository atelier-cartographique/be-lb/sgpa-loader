from datetime import datetime, date
import json
from django.contrib.gis.geos.geometry import GEOSGeometry


def serialize_nullable(ser):
    def inner(value):
        if value is None:
            return None
        return ser(value)

    return inner


def serialize_lingua(r):
    return r.to_dict()


def geojson(g: GEOSGeometry):
    return json.loads(g.geojson, parse_float=lambda x: round(float(x), 2))


def serialize_datetime(dt: datetime):
    return int(dt.timestamp() * 1000)

def serialize_date(d: date):
    return serialize_datetime(datetime(d.year,d.month,d.day))

def deserialize_datetime(ts):
    return datetime.fromtimestamp(ts / 1000)


nullable_lingua = serialize_nullable(serialize_lingua)


def serialize_nullable_obj(obj) -> str:
    return nullable_lingua(obj.label) if obj else None


def serialize_nullable_multi_obj(obj):
    if obj:
        return [nullable_lingua(o.label) for o in obj.all()]
    return None


def serialize_user(u):
    return u.username


def serialize_history(obj):
    if obj:
        value_field = [
            field.name
            for field in obj._meta.fields
            if field.name not in ("validity", "tree", "report", "id")
        ][0]
        if value_field:
            return {
                "validity": serialize_datetime(obj.validity.lower),
                "value": getattr(obj, value_field),
            }


def serialize_nullable_history_obj(obj):
    if obj:
        value_field = [
            field.name
            for field in obj._meta.fields
            if field.name not in ("validity", "tree", "report", "id")
        ][0]
        if value_field:
            return {
                "validity": serialize_datetime(obj.validity.lower),
                "value": getattr(obj, value_field).id,
            }


def serialize_nullable_history_multi_obj(obj, value_field):
    if obj:
        return {
            "validity": serialize_datetime(obj.validity.lower),
            "value": [o.id for o in getattr(obj, value_field).all()],
        }
