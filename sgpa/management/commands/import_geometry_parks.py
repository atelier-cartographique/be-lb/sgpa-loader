from django.core.management.base import BaseCommand
from django.db import connection
import json
from django.utils.text import slugify
from sgpa.models import Park
from django.contrib.gis.geos import GEOSGeometry
from django.contrib.gis.geos import Polygon, MultiPolygon

def ident(row):
    return str(row["gid"]) + '_' + row["code_site"].upper()

def lang_rec(row):
    fr = row["nom"]
    nl = row["naam"]
    en = ''

    fr = fr if fr else en if en else nl
    nl = nl if nl else en if en else fr

    return dict(fr=fr, nl=nl)

class Command(BaseCommand):
    def handle(self, *args, **options):
        # load data from geojson file
        with open('../../sgpa-loader/sgpa/data/parcs_bxl.geojson') as file:
            data = json.load(file)

            # Delete all current records because otherwise cannot delete parameters
            Park.objects.all().delete()

            for row in data["features"]:
                if row["properties"]["code_site"] is not None:
                    Park.objects.create(
                        id = ident(row["properties"]),
                        trigramme = row["properties"]["code_site"],
                        name = lang_rec(row["properties"]),
                        multipolygon = GEOSGeometry(str(row["geometry"])),
                    )

        print('JOB DONE.')