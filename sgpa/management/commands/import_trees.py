import csv
from difflib import SequenceMatcher

from django.core.management.base import BaseCommand
from django.core.exceptions import ObjectDoesNotExist
from django.utils.datetime_safe import datetime
from datetime import timedelta

from psycopg2._range import DateRange

from sgpa.models.tree.tree_legacy import TreeLegacy
from sgpa.models import (
    Tree,
    TreeKind,
    TreeStatus,
    Dendrometry,
    TreeEnvironment,
    TreeReport,
    TreeSymptomBranch,
    TreeSymptomCollar,
    TreeSymptomLeave,
    TreeSymptomRoot,
    TreeSymptomTrunk,
    TreePathogen,
    TreePest,
    TreeEpiphyte,
    Recommendation,
    RecommendationKind,
    RiskAnalysis,
    TreeReportGroup,
    TreeStructure,
    Park,
    Permit,
    Work,
    WORK_STATE_COMPLETED,
    WorkTeam,
    WorkKind,
    WorkTree,
    STATE_ACCEPTED,
    WORK_TREE_STATE_PENDING,
    WORK_TREE_STATE_COMPLETED,
    CachedTree,
)
from sgpa.models.tree.classification import get_tree_kind_hierarchy
from sgpa.serializers import serialize_tree
import logging
from typing import Optional, List

"""
First, get a SQL dump of the trees database and import it in the db in a table named as "sgpa_tree_legacy"

eg. `psql -h localhost -p 5434 -U postgres cartostation -f dump.sql`

The model `TreeLegacy` is used to read from this table of imported trees. This model was generated using the inspectdb command.

Run the command as `python manage.py import_trees`

"""

# logger = logging.getLogger('sgpa')  # TODO logging to a file does not work like this, should be integrated with django logging
logger = logging.getLogger(__name__)

ONTOGENESIS_JEUNE = 1
ONTOGENESIS_ADULTE = 2
ONTOGENESIS_MATURE = 3
ONTOGENESIS_SENESCENT = 4


def str2list(s: str) -> List[str]:
    strs = [s.strip() for s in s.replace("{", "").replace("}", "").split(",")]
    strs = [s.strip('"').strip() for s in strs]
    return filter(lambda r: not (r == "" or r == ","), strs)


def get_ontogenesis(tl: TreeLegacy) -> Optional[int]:
    if tl.ontogenese == "Jeune":
        return ONTOGENESIS_JEUNE
    elif tl.ontogenese == "Adulte":
        return ONTOGENESIS_ADULTE
    elif tl.ontogenese == "Mature":
        return ONTOGENESIS_MATURE
    elif tl.ontogenese == "Senescent":
        return ONTOGENESIS_SENESCENT
    else:
        return None


def get_dendrometry(tl: TreeLegacy) -> Optional[Dendrometry]:
    if tl.h_tot_m and tl.c150_cm and tl.nb_brins and tl.d_cour_m:
        return Dendrometry(
            height=tl.h_tot_m,
            circumference150=tl.c150_cm,
            branches_count=tl.nb_brins,
            crown_diameter_meter=tl.d_cour_m,
        )


properties = dict()


class NotAllPropertiesFoundException(Exception):
    def __init__(self, model, values):
        self.model = model
        self.values = values


def extract_date(date_str: str) -> Optional[datetime.date]:
    if date_str is None:
        return None

    try:
        date = datetime.strptime(date_str[0:10], "%Y-%m-%d").date()
    except ValueError as e_1:
        try:
            date = datetime.strptime(date_str[0:10], "%Y_%m_%d").date()
        except ValueError:
            raise e_1

    return date


def extract_delay(delay: str, f: datetime.date):
    if delay is None:
        return None
    if f is None:
        return None
    sanitized = delay.strip()

    if sanitized == "":
        return None
    elif sanitized == "> 3 ans":
        return DateRange(lower=f + timedelta(days=365 * 3), upper=None)
    elif sanitized == "< 1 an":
        return DateRange(lower=f, upper=f + timedelta(days=365))
    elif sanitized == "< 3 ans":
        return DateRange(lower=f, upper=f + timedelta(days=365 * 3))
    elif sanitized == "< 1 mois":
        return DateRange(lower=f, upper=f + timedelta(weeks=4))

    raise ValueError(f"Invalid delay format: {delay}")


def get_property_list(model, records: List[str]):
    if properties.get(model) is None:
        properties[model] = model.objects.all()

    # filter to remove annoying values
    given = set(records)
    remaining = set(given)
    result = set()

    # algo to search within properties
    def compare_model_and_given(model_instance, compare) -> bool:
        if isinstance(model_instance.label, dict):
            if "fr" in model_instance.label:
                label_fr = model_instance.label["fr"].strip().lower()
            else:
                return False
        else:
            label_fr = model_instance.strip().lower()

        if label_fr.strip() == "":
            return False

        compare_lower = compare.lower()

        if compare_lower in label_fr:
            return True

        if "blessure" in compare_lower and not (
            "moyenne" in compare_lower
            or "faible" in compare_lower
            or "élevée" in compare_lower
        ):
            if "10 < Ø < 20".lower() in compare_lower:
                return compare_model_and_given(model_instance, "blessure moyenne")
            elif "Ø > 20 cm".lower() in compare_lower:
                return compare_model_and_given(model_instance, "blessure élevée")
            elif "Ø < 10 cm".lower() in compare_lower:
                return compare_model_and_given(model_instance, "blessure faible")
            else:
                logger.info("a blessure, but dimension not set: " + compare_lower)

        # compare sequences
        s = SequenceMatcher(None, label_fr.lower(), compare_lower)

        return s.ratio() > 0.90

    for record in given:
        for m in properties[model]:
            if not compare_model_and_given(m, record):
                continue
            try:
                remaining.remove(record)
            except KeyError as e:
                logger.error(
                    f"key error, remaining keys are {remaining}, record: {record}, records: {records}, given_filtered: {given}, model: {model}"
                )
            result.add(m)

    return list(result), list(remaining)


def extract_risk(r: str) -> int:
    return r.split(")")[0][-1] if r else None


def get_risk_analysis(tl: TreeLegacy) -> Optional[RiskAnalysis]:
    break_risk = extract_risk(tl.secu_risq)
    size_risk = extract_risk(tl.secu_calib)
    target_risk = extract_risk(tl.secu_cible)

    if break_risk and size_risk and target_risk:
        return RiskAnalysis(
            break_risk=break_risk, size_risk=size_risk, target_risk=target_risk
        )


def get_park(trigramme):
    if trigramme is None:
        return None
    try:
        return Park.objects.get(trigramme=trigramme)
    except Exception:
        return None


def get_workteam(workteam: str) -> Optional[WorkTeam]:
    if workteam is None:
        return None
    elif workteam == "Eurosense":
        return WorkTeam.objects.get(pk=10000)
    elif workteam == "BE":
        return WorkTeam.objects.get(pk=10001)

    return None


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument(
            "--max",
            type=int,
            help="Specify a maximum amount of trees to import",
        )
        parser.add_argument(
            "--park",
            type=str,
            help="Only import trees from this park",
        )
        parser.add_argument(
            "--keep",
            action="store_true",
            help="Keep existing trees",
        )

    def handle(self, *args, **options):
        if options["keep"] is False:
            Tree.objects.all().delete()
            CachedTree.objects.all().delete()

        queryset = TreeLegacy.objects.all()

        if "park" in options:
            if options["park"]:
                queryset = queryset.filter(code_site=options["park"])
        if "max" in options:
            max_trees = options["max"]
            queryset = queryset[:max_trees]

        report_group = TreeReportGroup(date=datetime.now())
        report_group.save()

        work_group = Work(status=WORK_STATE_COMPLETED)
        work_group.save()

        with open("/tmp/unimported.csv", "w") as csvfile:
            not_imported_writer = csv.writer(csvfile)
            not_imported_writer.writerow(
                ["tree_id", "park_id", "tree_arbotag", "field", "data"]
            )

            for tl in queryset:
                logging.info(f"processing {tl.gid}")
                tree_kind = get_tree_kind_hierarchy(tl.essence)

                if tree_kind is None:
                    logging.error(
                        f"essence for trees replace by indeterminatum, original was: {tl.essence}"
                    )
                    not_imported_writer.writerow(
                        [tl.gid, tl.code_site, tl.id_arb, "essence", str(tl.essence)]
                    )
                    tree_kind = TreeKind.objects.get(pk=10000)

                remarkable_tree_id = None
                if tl.id_arb_rem:
                    if tl.id_arb_rem >= 0:
                        remarkable_tree_id = tl.id_arb_rem

                coupe = None
                if tl.coupe:
                    if tl.coupe >= 0:
                        coupe = tl.coupe

                tree = Tree(
                    id=tl.gid,
                    kind=tree_kind,
                    park=get_park(tl.code_site),
                    pin=tl.id_arb,
                    cut=coupe,
                    remarkable_tree_id=remarkable_tree_id,
                    point=tl.wkb_geometry,
                )
                tree.save()

                try:
                    report_date = extract_date(tl.date)
                except ValueError as e:
                    logging.error(f"date for tree not parsed: {e}")
                    not_imported_writer.writerow(
                        [tl.gid, tl.code_site, tl.id_arb, "date", tl.date]
                    )
                    report_date = None

                if report_date is not None:
                    report = TreeReport(
                        date=report_date,
                        tree=tree,
                        done_by=get_workteam(tl.controleur),
                        group=report_group,
                    )
                    report.save()
                else:
                    report = None

                # Set related data
                try:
                    tree.set_status(
                        TreeStatus.objects.get(label__fr=tl.statut), report=report
                    )
                except ObjectDoesNotExist:
                    not_imported_writer.writerow(
                        [tl.gid, tl.code_site, tl.id_arb, "statut", str(tl.statut)]
                    )
                    logging.error(f"status for tree {tl.gid} not imported: {tl.statut}")

                try:
                    ontogenesis = get_ontogenesis(tl)
                    if ontogenesis:
                        tree.set_ontogenesis(ontogenesis, report=report)
                except ObjectDoesNotExist:
                    not_imported_writer.writerow(
                        [
                            tl.gid,
                            tl.code_site,
                            tl.id_arb,
                            "ontogenesis",
                            str(ontogenesis),
                        ]
                    )
                    logging.error(f"ontogenesis for tree {tl.gid} not imported")

                try:
                    dendrometry = get_dendrometry(tl)
                    if dendrometry:
                        tree.set_dendrometry(dendrometry, report=report)
                except ObjectDoesNotExist:
                    not_imported_writer.writerow(
                        [tl.gid, tl.code_site, tl.id_arb, "dendometry", ""]
                    )
                    logging.error(f"dendrometry for tree {tl.gid} not imported")

                try:
                    tree.set_structure(
                        TreeStructure.objects.get(label__fr=tl.structure), report=report
                    )
                except ObjectDoesNotExist:
                    not_imported_writer.writerow(
                        [tl.gid, tl.code_site, tl.id_arb, "structure", tl.structure]
                    )
                    logging.error(f"structure for tree {tl.gid} not imported")

                if tl.envi:
                    props, remaining = get_property_list(
                        TreeEnvironment, str2list(tl.envi)
                    )
                    if props:
                        tree.set_environment(props, report=report)
                    if len(remaining) > 0:
                        logging.error(
                            f"environment for tree {tl.gid} not imported: {remaining}"
                        )
                        for v in remaining:
                            not_imported_writer.writerow(
                                [tl.gid, tl.code_site, tl.id_arb, "environment", v]
                            )

                try:
                    tree.set_vitality(int(tl.etat_sanit * 10), report=report)
                except Exception:
                    not_imported_writer.writerow(
                        [
                            tl.gid,
                            tl.code_site,
                            tl.id_arb,
                            "vitality",
                            str(tl.etat_sanit),
                        ]
                    )
                    logging.error(f"vitality for tree {tl.gid} not imported")

                if tl.sympt_raci:
                    props, remaining = get_property_list(
                        TreeSymptomRoot, str2list(tl.sympt_raci)
                    )
                    if props:
                        tree.set_symptom_root(props, report=report)
                    if len(remaining) > 0:
                        logging.error(
                            f"symptom root for tree {tl.gid} not imported: {remaining}"
                        )
                        for v in remaining:
                            not_imported_writer.writerow(
                                [tl.gid, tl.code_site, tl.id_arb, "sympt_raci", v]
                            )

                if tl.sympt_coll:
                    props, remaining = get_property_list(
                        TreeSymptomCollar, str2list(tl.sympt_coll)
                    )
                    if props:
                        tree.set_symptom_collar(props, report=report)
                    if len(remaining) > 0:
                        for v in remaining:
                            not_imported_writer.writerow(
                                [tl.gid, tl.code_site, tl.id_arb, "symptom_coll", v]
                            )
                        logging.error(
                            f"symptom collar for tree {tl.gid} not imported: {remaining}"
                        )

                if tl.sympt_tron:
                    props, remaining = get_property_list(
                        TreeSymptomTrunk, str2list(tl.sympt_tron)
                    )
                    if props:
                        tree.set_symptom_trunk(props, report=report)
                    if len(remaining) > 0:
                        for v in remaining:
                            not_imported_writer.writerow(
                                [tl.gid, tl.code_site, tl.id_arb, "sympt_tron", v]
                            )
                        logging.error(
                            f"symptom tron for tree {tl.gid} not imported: {remaining}"
                        )

                if tl.sympt_bran:
                    props, remaining = get_property_list(
                        TreeSymptomBranch, str2list(tl.sympt_bran)
                    )
                    if props:
                        tree.set_symptom_branch(props, report=report)
                    if len(remaining) > 0:
                        logging.error(
                            f"symptom branches for tree {tl.gid} not imported: {remaining}"
                        )
                        for v in remaining:
                            not_imported_writer.writerow(
                                [tl.gid, tl.code_site, tl.id_arb, "sympt_branch", v]
                            )

                if tl.sympt_feui:
                    props, remaining = get_property_list(
                        TreeSymptomLeave, str2list(tl.sympt_feui)
                    )
                    if props:
                        tree.set_symptom_leave(props, report=report)
                    if len(remaining) > 0:
                        for v in remaining:
                            not_imported_writer.writerow(
                                [tl.gid, tl.code_site, tl.id_arb, "sympt_feui", v]
                            )
                        logging.error(
                            f"symptom feui for tree {tl.gid} not imported: {remaining}"
                        )

                if tl.pathogene:
                    props, remaining = get_property_list(
                        TreePathogen, str2list(tl.pathogene)
                    )
                    if props:
                        tree.set_pathogen(props, report=report)
                    if len(remaining) > 0:
                        for v in remaining:
                            not_imported_writer.writerow(
                                [tl.gid, tl.code_site, tl.id_arb, "pathogene", v]
                            )
                        logging.error(
                            f"pathogen for tree {tl.gid} not imported: {remaining}"
                        )

                if tl.ravageur:
                    props, remaining = get_property_list(
                        TreePest, str2list(tl.ravageur)
                    )
                    if props:
                        tree.set_pest(props, report=report)
                    if len(remaining) > 0:
                        for v in remaining:
                            not_imported_writer.writerow(
                                [tl.gid, tl.code_site, tl.id_arb, "ravageur", v]
                            )
                        logging.error(
                            f"pest for tree {tl.gid} not imported: {remaining}"
                        )

                if tl.epiphyte:
                    props, remaining = get_property_list(
                        TreeEpiphyte, str2list(tl.epiphyte)
                    )
                    if props:
                        tree.set_epiphyte(props, report=report)
                    if len(remaining) > 0:
                        for v in remaining:
                            not_imported_writer.writerow(
                                [tl.gid, tl.code_site, tl.id_arb, "epiphyte", v]
                            )
                        logging.error(
                            f"epiphyte for tree {tl.gid} not imported: {remaining}"
                        )

                try:
                    risk_analysis = get_risk_analysis(tl)
                    if risk_analysis:
                        tree.set_risk_analysis(risk_analysis, report=report)
                except Exception:
                    logging.error(f"risk analysis for tree {tl.gid} not imported")

                if tl.cons_intrv != "" and tl.cons_intrv is not None:
                    recommendationKinds, remaining = get_property_list(
                        RecommendationKind, str2list(tl.cons_intrv)
                    )

                    if len(recommendationKinds) > 0:
                        try:
                            for r in recommendationKinds:
                                recommendation = Recommendation(
                                    tree=tree,
                                    report=report,
                                    # take care: if no date, replaced by "day of import"
                                    date=(
                                        report.date
                                        if report is not None
                                        else datetime.now()
                                    ),
                                    recommendation=r,
                                    application_range=extract_delay(
                                        tl.cons_delai,
                                        (
                                            report.date
                                            if report is not None
                                            else datetime.now()
                                        ),
                                    ),
                                )

                                recommendation.save()
                        except ValueError as e:
                            logging.error(
                                f"could not parse recommendation delay: {e}", e
                            )
                            not_imported_writer.writerow(
                                [
                                    tl.gid,
                                    tl.code_site,
                                    tl.id_arb,
                                    "recommendation_delay",
                                    tl.cons_delai,
                                ]
                            )

                    if len(remaining) > 0:
                        for v in remaining:
                            not_imported_writer.writerow(
                                [tl.gid, tl.code_site, tl.id_arb, "recommendation", v]
                            )
                        logging.error(
                            f"recommendation for tree {tl.gid} not imported: {remaining}"
                        )
                else:
                    recommendation = None

                if tl.trait_type:
                    traitements, remaining = get_property_list(
                        WorkKind, str2list(tl.trait_type)
                    )

                    for v in remaining:
                        logging.error(f"traitement for tree {tl.gid} not imported: {v}")
                        not_imported_writer.writerow(
                            [tl.gid, tl.code_site, tl.id_arb, "traitment", v]
                        )

                    for traitement in traitements:
                        try:
                            date_w = extract_date(tl.trait_date)
                        except ValueError as e:
                            date_w = None
                            not_imported_writer.writerow(
                                [
                                    tl.gid,
                                    tl.code_site,
                                    tl.id_arb,
                                    "trait_date",
                                    tl.trait_date,
                                ]
                            )

                        if tl.autori_ref is not None and tl.autori_ref != "":
                            try:
                                date_p = extract_date(tl.autori_dat)
                            except ValueError as e:
                                date_p = None
                                not_imported_writer.writerow(
                                    [
                                        tl.gid,
                                        tl.code_site,
                                        tl.id_arb,
                                        "autori_dat",
                                        tl.autori_dat,
                                    ]
                                )

                            permit = Permit(
                                reference=tl.autori_ref,
                                date_answer=date_p,
                                state=STATE_ACCEPTED,
                            )
                            permit.save()
                        else:
                            permit = None

                        w = WorkTree(
                            tree=tree,
                            work=work_group,
                            work_kind=traitement,
                            done_date=(
                                date_w
                                if date_w is not None and date_w > datetime.now()
                                else None
                            ),
                            comment=str(tl.trait_rem),
                            permit=permit,
                            state=(
                                WORK_TREE_STATE_COMPLETED
                                if date_w is not None and date_w > datetime.now()
                                else WORK_TREE_STATE_PENDING
                            ),
                            recommendation=recommendation,
                        )
                        w.save()

                CachedTree.objects.create(
                    id=tree.id, data=serialize_tree(Tree.objects.get(pk=tree.id))
                )
