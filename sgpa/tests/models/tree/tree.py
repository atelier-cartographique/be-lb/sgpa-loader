from django.test import TestCase
from decimal import Decimal

from sgpa.models import (TreeKind, Tree, TreeStatus, Dendrometry,
                         TreeEnvironment, TreeSymptomRoot, TreeSymptomTrunk,
                         TreeSymptomLeave, TreePathogen, TreePest, TreeEpiphyte, RiskAnalysis, TreeStructure)


class TestTreeHistories(TestCase):
    def test_status_history(self):
        tree_kind = TreeKind(common_name="test")
        tree_kind.save()
        tree = Tree(kind=tree_kind)
        tree.save()
        status1 = TreeStatus(label={"fr": "test1"})
        status1.save()
        status2 = TreeStatus(label={"fr": "test2"})
        status2.save()

        self.assertIsNone(tree.current_status)
        self.assertIsNone(tree.current_status_history)
        self.assertEqual(0, len(tree.status_history.all()))

        tree.set_status(status1)
        self.assertEquals(status1, tree.current_status)
        self.assertEqual(1, len(tree.status_history.all()))
        self.assertTrue(tree.current_status_history.validity.upper_inf)

        tree.set_status(status2)
        self.assertEquals(status2, tree.current_status)
        self.assertEqual(2, len(tree.status_history.all()))
        self.assertTrue(tree.current_status_history.validity.upper_inf)

    def test_ontogenesis_history(self):
        tree_kind = TreeKind(common_name="test")
        tree_kind.save()
        tree = Tree(kind=tree_kind)
        tree.save()

        self.assertIsNone(tree.current_ontogenesis)
        self.assertIsNone(tree.current_ontogenesis_history)
        self.assertEqual(0, len(tree.ontogenesis_history.all()))

        tree.set_ontogenesis(1)

        self.assertEqual(1, tree.current_ontogenesis)
        self.assertTrue(tree.current_ontogenesis_history.validity.upper_inf)

    def test_dendrometry_history(self):
        tree_kind = TreeKind(common_name="test")
        tree_kind.save()
        tree = Tree(kind=tree_kind)
        tree.save()

        self.assertIsNone(tree.current_dendrometry)
        self.assertIsNone(tree.current_dendrometry_history)
        self.assertEqual(0, tree.dendrometry_history.count())

        dendrometry = Dendrometry(height=15.0, circumference150=120.10,
                                crown_diameter_meter=Decimal('5.0'), branches_count=1)

        tree.set_dendrometry(dendrometry)

        self.assertEqual(1, tree.dendrometry_history.count())
        self.assertEqual(15.0, tree.current_dendrometry.height)
        self.assertEqual(Decimal('120.100'), tree.current_dendrometry.circumference150)

    def test_structure_history(self):
        tree_kind = TreeKind(common_name="test")
        tree_kind.save()
        tree = Tree(kind=tree_kind)
        tree.save()

        self.assertIsNone(tree.current_epiphyte)
        p = TreeStructure(label={"fr": "test"})
        p.save()

        tree.set_structure(p)

        self.assertEqual(1, tree.structure_history.count())
        self.assertEqual(p, tree.current_structure)

    def test_environment_history(self):
        tree_kind = TreeKind(common_name="test")
        tree_kind.save()
        tree = Tree(kind=tree_kind)
        tree.save()
        tree_environment1 = TreeEnvironment(label={"fr": "test"})
        tree_environment1.save()
        tree_environment2 = TreeEnvironment(label={"fr": "test"})
        tree_environment2.save()

        self.assertIsNone(tree.current_environment)
        self.assertIsNone(tree.current_environment_history)
        self.assertEqual(0, tree.environment_history.count())

        tree.set_environment([tree_environment1, tree_environment2])

        self.assertEqual(1, tree.environment_history.count())
        self.assertTrue(tree.current_environment_history.validity.upper_inf)

    def test_vitality_history(self):
        tree_kind = TreeKind(common_name="test")
        tree_kind.save()
        tree = Tree(kind=tree_kind)
        tree.save()

        self.assertIsNone(tree.current_vitality)

        tree.set_vitality(1)

        self.assertEqual(1, tree.current_vitality)
        self.assertEqual(1, tree.vitality_history.count())

    def test_symptom_root_history(self):
        tree_kind = TreeKind(common_name="test")
        tree_kind.save()
        tree = Tree(kind=tree_kind)
        tree.save()

        self.assertIsNone(tree.current_symptom_root)
        symptom_root = TreeSymptomRoot(label={"fr": "test"})
        symptom_root.save()

        tree.set_symptom_root(symptom_root)

        self.assertEqual(1, tree.symptom_root_history.count())
        self.assertEqual(symptom_root, tree.current_symptom_root)

    def test_symptom_trunk_history(self):
        tree_kind = TreeKind(common_name="test")
        tree_kind.save()
        tree = Tree(kind=tree_kind)
        tree.save()

        self.assertIsNone(tree.current_symptom_trunk)
        symptom = TreeSymptomTrunk(label={"fr": "test"})
        symptom.save()

        tree.set_symptom_trunk(symptom)

        self.assertEqual(1, tree.symptom_trunk_history.count())
        self.assertEqual(symptom, tree.current_symptom_trunk)

    def test_symptom_leave_history(self):
        tree_kind = TreeKind(common_name="test")
        tree_kind.save()
        tree = Tree(kind=tree_kind)
        tree.save()

        self.assertIsNone(tree.current_symptom_leave)
        symptom = TreeSymptomLeave(label={"fr": "test"})
        symptom.save()

        tree.set_symptom_leave(symptom)

        self.assertEqual(1, tree.symptom_leave_history.count())
        self.assertEqual(symptom, tree.current_symptom_leave)

    def test_pathogen_history(self):
        tree_kind = TreeKind(common_name="test")
        tree_kind.save()
        tree = Tree(kind=tree_kind)
        tree.save()

        self.assertIsNone(tree.current_pathogen)
        p = TreePathogen(label={"fr": "test"})
        p.save()

        tree.set_pathogen(p)

        self.assertEqual(1, tree.pathogen_history.count())
        self.assertEqual(p, tree.current_pathogen)

    def test_pest_history(self):
        tree_kind = TreeKind(common_name="test")
        tree_kind.save()
        tree = Tree(kind=tree_kind)
        tree.save()

        self.assertIsNone(tree.current_pathogen)
        p = TreePest(label={"fr": "test"})
        p.save()

        tree.set_pest(p)

        self.assertEqual(1, tree.pest_history.count())
        self.assertEqual(p, tree.current_pest)

    def test_epiphyte_history(self):
        tree_kind = TreeKind(common_name="test")
        tree_kind.save()
        tree = Tree(kind=tree_kind)
        tree.save()

        self.assertIsNone(tree.current_epiphyte)
        p = TreeEpiphyte(label={"fr": "test"})
        p.save()

        tree.set_epiphyte(p)

        self.assertEqual(1, tree.epiphyte_history.count())
        self.assertEqual(p, tree.current_epiphyte)

    def test_risk_analysis_history(self):
        tree_kind = TreeKind(common_name="test")
        tree_kind.save()
        tree = Tree(kind=tree_kind)
        tree.save()

        self.assertIsNone(tree.current_risk_analysis)

        r = RiskAnalysis(1, 2, 3)

        tree.set_risk_analysis(r)

        self.assertEqual(1, tree.risk_analysis_history.count())
        self.assertEqual(2, tree.current_risk_analysis.size_risk)
