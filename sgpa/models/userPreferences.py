from django.contrib.gis.db import models
from django.contrib.auth.models import User
from . import Park
import uuid

class UserPreferences(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    user = models.ForeignKey(User, on_delete=models.PROTECT, null=False, blank=False)
    parks = models.ManyToManyField(Park)