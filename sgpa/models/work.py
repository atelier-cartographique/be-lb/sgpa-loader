from decimal import Decimal
from django.contrib.gis.db import models
from lingua.fields import text_field
from .tree import Tree, TreeStatus
from .recommendation import RecommendationKind, Recommendation
from .permit import Permit
from .team import WorkTeam
from .tree.classification import label_repr

PRIORITY_NORMAL = 'normal'
PRIORITY_IMMEDIATE = 'immediate'
PRIORITY_URGENT = 'high'

WORK_TREE_STATE_PENDING = 'pending'
WORK_TREE_STATE_IN_PROGRESS = 'in_progress'
WORK_TREE_STATE_COMPLETED = 'completed'
WORK_TREE_STATE_CHECKED = 'checked'
WORK_TREE_STATE_CANCELLED = 'cancelled'

WORK_STATE_DRAFT = 'draft'
WORK_STATE_PENDING = 'pending'
WORK_STATE_ACCEPTED = 'accepted'
WORK_STATE_REJECTED = 'rejected'
WORK_STATE_COMPLETED = 'completed'
WORK_STATE_CANCELLED = 'cancelled'


class WorkKind(models.Model):
    """Type de traitement"""
    label = text_field("work_kind_label")
    need_permit = models.BooleanField(default=False)
    active = models.BooleanField(default=False)
    possibility_of = models.ForeignKey(RecommendationKind, on_delete=models.RESTRICT, null=True)
    tree_status_after = models.ForeignKey(TreeStatus, on_delete=models.RESTRICT, null=True)

    def __str__(self) -> str:
        return label_repr(self)


class WorkKindBudget(models.Model):
    """Prix des types de traitement"""
    label = text_field("work_kind_budget_label")
    price = models.DecimalField(max_digits=10, decimal_places=2, default=Decimal('0.00'))
    code = models.TextField(default="", blank=True)

    def __str__(self) -> str:
        return label_repr(self)


class TreeCuttingDisposal(models.Model):
    """Destination des produits de coupe"""
    label = text_field("tree_cutting_disposal_label")

    def __str__(self) -> str:
        return label_repr(self)


class Work(models.Model):
    """Chantier"""
    done_by = models.ForeignKey(WorkTeam, on_delete=models.RESTRICT, null=True)
    status = models.CharField(max_length=20, default=WORK_STATE_DRAFT)


class WorkTree(models.Model):
    """Traitement sur un arbre"""
    tree = models.ForeignKey(Tree, on_delete=models.CASCADE, null=False)
    work = models.ForeignKey(Work, on_delete=models.RESTRICT)
    priority = models.CharField(max_length=20, default=PRIORITY_NORMAL)
    work_kind = models.ForeignKey(WorkKind, on_delete=models.RESTRICT, null=True)
    due_date = models.DateField(null=True)
    target_date = models.DateField(null=True)
    """Date prévisionnelle"""
    done_date = models.DateField(null=True)
    state = models.CharField(max_length=20, default=WORK_TREE_STATE_PENDING)
    permit = models.ForeignKey(Permit, on_delete=models.CASCADE, null=True)
    comment = models.TextField(default="", blank=True)
    price = models.DecimalField(max_digits=10, decimal_places=2, default=Decimal('0.00'))
    recommendation = models.ForeignKey(Recommendation, on_delete=models.RESTRICT, null=True)
    tree_cutting_disposal = models.ForeignKey(TreeCuttingDisposal, on_delete=models.RESTRICT, null=True)
    validation_comment = models.TextField(default="", blank=True)
