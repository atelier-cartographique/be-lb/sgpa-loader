from django.contrib.gis.db import models

from lingua.fields import text_field


class WorkTeam(models.Model):
    label = text_field("tree_report_work_team_label")
    can_recommendation = models.BooleanField(default=False)
    can_work = models.BooleanField(default=False)
    is_internal = models.BooleanField(default=False)
