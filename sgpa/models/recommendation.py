from django.contrib.gis.db import models
from django.contrib.postgres.fields import DateRangeField

from lingua.fields import text_field
from .tree import Tree
from .report import TreeReport
from .tree.classification import label_repr

class RecommendationKind(models.Model):
    label = text_field("recommandation_kind_label")
    needs_work = models.BooleanField(default=False)

    def __str__(self) -> str:
        return label_repr(self)

class Recommendation(models.Model):
    tree = models.ForeignKey(Tree, on_delete=models.RESTRICT)
    date = models.DateField()
    recommendation = models.ForeignKey(RecommendationKind, on_delete=models.RESTRICT)
    do_not_follow = models.BooleanField(default=False)
    application_range = DateRangeField(default=None, null=True)
    report = models.ForeignKey(TreeReport, on_delete=models.CASCADE, null=True)
