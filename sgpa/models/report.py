from django.contrib.gis.db import models
from django.utils.timezone import now
from .team import WorkTeam


class TreeReportGroup(models.Model):
    date = models.DateField()


class TreeReport(models.Model):
    """
    A report about a tree
    """

    date = models.DateField(default=now)
    tree = models.ForeignKey('sgpa.Tree', on_delete=models.CASCADE, null=True)
    done_by = models.ForeignKey(WorkTeam, on_delete=models.RESTRICT, null=True)
    group = models.ForeignKey('sgpa.TreeReportGroup', on_delete=models.RESTRICT, null=True)
