from django.contrib.gis.db import models
from django.contrib.auth.models import User
from . import Park, Tree

class Selection(models.Model):
    name = models.CharField(max_length=255, blank=True, default="")
    date_created = models.DateTimeField(auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)
    parks = models.ManyToManyField(Park)
    trees = models.ManyToManyField(Tree)
    user = models.ForeignKey(User, on_delete=models.PROTECT, related_name="+")
    shortcut = models.BooleanField(default=False)

    def __str__(self) -> str:
        return self.name
