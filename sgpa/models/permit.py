from django.contrib.gis.db import models

STATE_PENDING = 'pending'
STATE_ACCEPTED = 'accepted'
STATE_REJECTED = 'rejected'


class Permit(models.Model):
    reference = models.CharField(max_length=255, blank=True, default="")
    date_answer = models.DateField(null=True)
    date_introduced = models.DateField(null=True)
    state = models.CharField(max_length=20, default=STATE_PENDING)
    comment = models.TextField(blank=True, default="")
