from django.db import models
from lingua.fields import label_field
from django.contrib.gis.db import models
from django.contrib.auth.models import User

import uuid


class ParkColor(models.Model):
    color = models.CharField(max_length=25)
    name = models.CharField(max_length=25, null=True, blank=True)

    class Meta:
        db_table = "sgpa_park_color"
        verbose_name = "Park color"
        verbose_name_plural = "Park colors"

    def __str__(self) -> str:
        return str(self.name)


class Park(models.Model):
    id = models.CharField(max_length=64, primary_key=True)
    trigramme = models.CharField(max_length=3)
    name = label_field("park_name")
    multipolygon = models.MultiPolygonField(srid=31370, null=True, blank=True)
    color = models.ForeignKey(
        ParkColor, on_delete=models.CASCADE, null=True, blank=True
    )

    class Meta:
        db_table = "sgpa_park"
        verbose_name = "Park"
        verbose_name_plural = "Parks"

    def __str__(self) -> str:
        return str(self.name)
