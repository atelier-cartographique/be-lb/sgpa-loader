# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.contrib.gis.db import models


class TreeLegacy(models.Model):
    wkb_geometry = models.PointField(srid=31370, blank=True, null=True)
    ogc_fid = models.BigIntegerField(blank=True, null=True)
    gid = models.IntegerField(blank=True, null=False, primary_key=True)
    controleur = models.CharField(max_length=30, blank=True, null=True)
    date = models.CharField(max_length=254, blank=True, null=True)
    code_site = models.CharField(max_length=5, blank=True, null=True)
    proprio = models.CharField(max_length=50, blank=True, null=True)
    id_arb = models.BigIntegerField(blank=True, null=True)
    id_manque = models.CharField(max_length=3, blank=True, null=True)
    id_arb_old = models.BigIntegerField(blank=True, null=True)
    id_arb_rem = models.BigIntegerField(blank=True, null=True)
    x = models.FloatField(blank=True, null=True)
    y = models.FloatField(blank=True, null=True)
    coupe = models.BigIntegerField(blank=True, null=True)
    essence = models.CharField(max_length=100, blank=True, null=True)
    statut = models.CharField(max_length=30, blank=True, null=True)
    c150_cm = models.BigIntegerField(blank=True, null=True)
    h_tot_m = models.BigIntegerField(blank=True, null=True)
    d_cour_m = models.BigIntegerField(blank=True, null=True)
    nb_brins = models.BigIntegerField(blank=True, null=True)
    ontogenese = models.CharField(max_length=10, blank=True, null=True)
    age_approx = models.BigIntegerField(blank=True, null=True)
    structure = models.CharField(max_length=20, blank=True, null=True)
    envi = models.CharField(max_length=254, blank=True, null=True)
    sympt_raci = models.CharField(max_length=254, blank=True, null=True)
    sympt_coll = models.CharField(max_length=254, blank=True, null=True)
    sympt_tron = models.CharField(max_length=254, blank=True, null=True)
    sympt_bran = models.CharField(max_length=254, blank=True, null=True)
    sympt_feui = models.CharField(max_length=254, blank=True, null=True)
    pathogene = models.CharField(max_length=254, blank=True, null=True)
    ravageur = models.CharField(max_length=254, blank=True, null=True)
    epiphyte = models.CharField(max_length=254, blank=True, null=True)
    etat_sanit = models.FloatField(blank=True, null=True)
    pronostic = models.CharField(max_length=30, blank=True, null=True)
    secu_risq = models.CharField(max_length=30, blank=True, null=True)
    secu_calib = models.CharField(max_length=30, blank=True, null=True)
    secu_cible = models.CharField(max_length=30, blank=True, null=True)
    secu_dang = models.CharField(max_length=30, blank=True, null=True)
    cons_intrv = models.CharField(max_length=254, blank=True, null=True)
    cons_delai = models.CharField(max_length=30, blank=True, null=True)
    remarques = models.CharField(max_length=254, blank=True, null=True)
    trait_type = models.CharField(max_length=254, blank=True, null=True)
    trait_date = models.CharField(max_length=254, blank=True, null=True)
    trait_rem = models.CharField(max_length=254, blank=True, null=True)
    autori_ref = models.CharField(max_length=50, blank=True, null=True)
    autori_dat = models.CharField(max_length=254, blank=True, null=True)
    photo_orga = models.CharField(max_length=254, blank=True, null=True)
    photo_envi = models.CharField(max_length=254, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'sgpa_tree_legacy'
