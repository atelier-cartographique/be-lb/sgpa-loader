from django.contrib.gis.db import models
from lingua.fields import text_field


class TreeOwner(models.Model):
    code = models.CharField(max_length=250, default='')
    name = text_field("sgpa_tree_owner_name")