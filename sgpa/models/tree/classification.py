from typing import Optional

from django.contrib.gis.db import models
from django.db.models import Q

from lingua.fields import nullable_text_field, text_field


def label_repr(self):
    return f"FR: {self.label.get('fr')} - NL: {self.label.get('nl')}"


class TreeKind(models.Model):
    """
    Tree species
    """

    label = models.TextField(default="")
    common_name = nullable_text_field("tree_kind_common_name")
    parent = models.ForeignKey("self", on_delete=models.CASCADE, null=True, blank=True)

    @property
    def latin_name(self) -> str:
        if self.parent:
            if self.parent.parent:
                return f"{self.parent.parent.label} {self.parent.label} '{self.label}'"
            else:
                return f"{self.parent.label} {self.label}"
        else:
            return f"{self.label} sp."

    def __str__(self) -> str:
        return self.latin_name


class TreeStatus(models.Model):
    """
    Tree status: if the tree is "debout", or "chandelle" or "souche".
    """

    label = text_field("tree_status_label")

    class Meta:
        verbose_name_plural = "Tree status"

    def __str__(self) -> str:
        return label_repr(self)


class TreeStructure(models.Model):
    """
    Tree structure

    examples: Libre, semi-libre, architecturée, mutilée, délaissée
    """

    label = text_field("tree_structure_label")

    def __str__(self) -> str:
        return label_repr(self)


class TreeEnvironment(models.Model):
    """
    Tree environment

    examples: Isolé, Lisière, Bois, ...
    """

    label = text_field("tree_environment_label")

    def __str__(self) -> str:
        return label_repr(self)


class TreeSymptomRoot(models.Model):
    label = text_field("tree_symptom_root_label")

    def __str__(self) -> str:
        return label_repr(self)


class TreeSymptomTrunk(models.Model):
    label = text_field("tree_symptom_trunk_label")

    def __str__(self) -> str:
        return label_repr(self)


class TreeSymptomLeave(models.Model):
    label = text_field("tree_symptom_leave_label")

    def __str__(self) -> str:
        return label_repr(self)


class TreeSymptomBranch(models.Model):
    label = text_field("tree_symptom_branch_label")

    def __str__(self) -> str:
        return label_repr(self)


class TreeSymptomCollar(models.Model):
    label = text_field("tree_symptom_collar_label")

    def __str__(self) -> str:
        return label_repr(self)


class TreePathogen(models.Model):
    label = text_field("tree_pathogene_label")

    def __str__(self) -> str:
        return label_repr(self)


class TreePest(models.Model):
    label = text_field("tree_pest_label")

    def __str__(self) -> str:
        return label_repr(self)


class TreeEpiphyte(models.Model):
    label = text_field("tree_epiphyte_label")

    def __str__(self) -> str:
        return label_repr(self)


def get_tree_kind_hierarchy(latin_name: str) -> Optional[TreeKind]:
    """
    Return the tree kind, performing some search within genre / espece / cultivar, and remove
    cultivar if not found
    """
    if latin_name:
        genuses = [
            g.strip().strip("'") for g in latin_name.strip().split(" ") if g != ""
        ]

        if len(genuses) == 1:
            kind_hierarchy = [genuses[0]]
        elif len(genuses) == 2:
            kind_hierarchy = [genuses[0], genuses[1]]
        elif len(genuses) == 3 and genuses[1] == "x":
            kind_hierarchy = [genuses[0], "x " + genuses[2]]
        elif len(genuses) >= 3:
            kind_hierarchy = [genuses[0], genuses[1], " ".join(genuses[2:])]
        else:
            raise ValueError(
                f"Invalid latin name: {latin_name}, length: {len(genuses)}"
            )

        if len(kind_hierarchy) == 1:
            qs = TreeKind.objects.filter(Q(label__icontains=kind_hierarchy[0]))
        elif len(kind_hierarchy) == 2:
            qs = TreeKind.objects.filter(
                Q(label__icontains=kind_hierarchy[1]),
                Q(parent__label__icontains=kind_hierarchy[0]),
            )
        else:
            qs = TreeKind.objects.filter(
                Q(label__icontains=kind_hierarchy[2]),
                Q(parent__label__icontains=kind_hierarchy[1]),
                Q(parent__parent__label__icontains=kind_hierarchy[0]),
            )

        if qs.exists():
            for tk in qs.all():
                if " ".join(genuses) == tk.latin_name.replace("'", "").replace(
                    " sp.", ""
                ):
                    return tk

    return None
