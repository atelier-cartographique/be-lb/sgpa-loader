from django.contrib.auth.models import User
from django.contrib.gis.db import models
from ..report import TreeReport


class Comment(models.Model):
    """A comment on a tree"""
    tree = models.ForeignKey('sgpa.Tree', on_delete=models.CASCADE)
    date = models.DateTimeField(auto_now_add=True)
    author = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
    content = models.TextField()
    report = models.ForeignKey(TreeReport, on_delete=models.CASCADE, null=True)
