from django.contrib.gis.db import models
from django.contrib.postgres.constraints import ExclusionConstraint
from django.contrib.postgres.fields import RangeOperators, DateTimeRangeField, DecimalRangeField, IntegerRangeField
from django.db.models import Deferrable
from ..report import TreeReport
from .classification import (TreeStatus, TreeStructure, TreeEnvironment,
                             TreeSymptomRoot, TreeSymptomTrunk, TreeSymptomBranch, TreeSymptomCollar,
                             TreeSymptomLeave, TreePathogen, TreePest, TreeEpiphyte)


def create_history_model(related_name):
    class History(models.Model):
        class Meta:
            abstract = True
            constraints = [
                ExclusionConstraint(
                    name="history_exclusion_" + related_name,
                    expressions=[
                        ('validity', RangeOperators.OVERLAPS),
                        ('tree', RangeOperators.EQUAL),
                    ],
                    deferrable=Deferrable.DEFERRED
                )
            ]

        tree = models.ForeignKey('sgpa.Tree',
                                 on_delete=models.CASCADE,
                                 related_name=related_name)
        validity = DateTimeRangeField()
        report = models.ForeignKey(TreeReport,
                                   on_delete=models.CASCADE,
                                   null=True)

    return History


class TreeStatusHistory(create_history_model("status_history")):
    """
    The history of statuses of trees
    """
    status = models.ForeignKey(TreeStatus, on_delete=models.RESTRICT)


class TreeOntogenesisHistory(create_history_model("ontogenesis_history")):
    """
    The history of ontogenesis of trees
    """

    ontogenesis = models.PositiveSmallIntegerField()


class TreeDendrometryHistory(create_history_model("dendrometry_history")):
    height = models.DecimalField(max_digits=10, decimal_places=3)
    """Height of the tree (in meters)"""

    circumference150 = models.DecimalField(max_digits=10, decimal_places=3)
    """Circumference of the tree at 150cm (in centimeters)"""

    crown_diameter_range = DecimalRangeField()
    """Diameter of the crown's tree, (in range of meters)"""

    branches_count = models.PositiveIntegerField(default=1)
    """Number of branches of 40cm circumference and at 150cm height"""


class TreeStructureHistory(create_history_model("structure_history")):
    """
    The history of Tree structure of trees
    """
    structure = models.ForeignKey(TreeStructure, on_delete=models.RESTRICT)


class TreeEnvironmentHistory(create_history_model("environment_history")):
    """
    The history of Tree environment of trees
    """
    environments = models.ManyToManyField(TreeEnvironment)


class TreeVitalityHistory(create_history_model("vitality_history")):
    """
     État sanitaire

     d'après le C.C.T 2015. Cahier des charges type relatif aux voiries en Régions de Bruxelles-Capitale, chapitre K :
     Plantations et engazonnement. Ministère de la Région de Bruxelles-Capitale, Administration de l'équipement et
     des déplacements – Direction des Voiries (Belgique), p.824. Disponible sur
     https://mobilite-mobiliteit.brussels/sites/default/files/cct2015fr.pdf.
    """
    vitality = models.PositiveSmallIntegerField(default=0)
    pronostic = IntegerRangeField(null=True, default=None)


class TreeSymptomRootHistory(create_history_model("symptom_root_history")):
    symptoms = models.ManyToManyField(TreeSymptomRoot)


class TreeSymptomTrunkHistory(create_history_model("symptom_trunk_history")):
    symptoms = models.ManyToManyField(TreeSymptomTrunk)


class TreeSymptomLeaveHistory(create_history_model("symptom_leave_history")):
    symptoms = models.ManyToManyField(TreeSymptomLeave)

class TreeSymptomBranchHistory(create_history_model("symptom_branch_history")):
    symptoms = models.ManyToManyField(TreeSymptomBranch)

class TreeSymptomCollarHistory(create_history_model("symptom_collar_history")):
    symptoms = models.ManyToManyField(TreeSymptomCollar)

class TreePathogenHistory(create_history_model("pathogen_history")):
    pathogens = models.ManyToManyField(TreePathogen)


class TreePestHistory(create_history_model("pest_history")):
    pests = models.ManyToManyField(TreePest)


class TreeEpiphyteHistory(create_history_model("epiphyte_history")):
    epiphytes = models.ManyToManyField(TreeEpiphyte)


class TreeRiskAnalysisHistory(create_history_model("risk_analysis_history")):
    break_risk = models.PositiveSmallIntegerField(null=False)
    """Risque de rupture / basculement"""

    size_risk = models.PositiveSmallIntegerField(null=False)
    """Diamètre de l'organe identifié comme dangereux"""

    target_risk = models.PositiveSmallIntegerField(null=False)
    """Présence d'une cible potentielle"""

    def hazard(self) -> int:
        return self.break_risk + self.size_risk + self.target_risk
