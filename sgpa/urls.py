# urls.py
from django.urls import path, reverse
from .views import (
    get_version,
    get_all_parks,
    get_trees_park,
    get_all_trees,
    get_all_trees_id,
    view_tree,
    edit_tree,
    create_tree,
    view_tree_recommendation,
    view_tree_work,
    view_selection,
    view_user_selections,
    get_trees_selection,
    create_selection,
    edit_selection,
    get_references,
    get_user_preferences,
    post_tree_image,
    get_tree_image,
)


def prefix(s, sep="/"):
    return f"sgpa{sep}{s}"


urlpatterns = [
    path(prefix("version/"), get_version, name=prefix("get_version", "-")),
    path(prefix("parks"), get_all_parks, name=prefix("get_all_parks", "-")),
    path(
        prefix("parks/<str:park_id>/trees"),
        get_trees_park,
        name=prefix("get_trees_park", "-"),
    ),
    path(prefix("trees-id"), get_all_trees_id, name=prefix("get_all_trees_id", "-")),
    path(prefix("trees"), get_all_trees, name=prefix("get_all_trees", "-")),
    path(prefix("trees/<int:tree_id>/"), view_tree, name=prefix("view_tree", "-")),
    path(prefix("trees/<int:tree_id>/edit"), edit_tree, name=prefix("edit_tree", "-")),
    path(prefix("trees/new"), create_tree, name=prefix("new_tree", "-")),
    path(
        prefix("trees/<int:tree_id>/recommendation"),
        view_tree_recommendation,
        name=prefix("view_tree_recommendation", "-"),
    ),
    path(
        prefix("trees/<int:tree_id>/work"),
        view_tree_work,
        name=prefix("view_tree_work", "-"),
    ),
    path(
        prefix("selection/<int:selection_id>"),
        view_selection,
        name=prefix("view_selection", "-"),
    ),
    path(
        prefix("selection/<int:selection_id>/trees"),
        get_trees_selection,
        name=prefix("get_trees_selection", "-"),
    ),
    path(
        prefix("selection/user/<int:user_id>"),
        view_user_selections,
        name=prefix("view_user_selections", "-"),
    ),
    path(prefix("selection/new"), create_selection, name=prefix("new_selection", "-")),
    path(
        prefix("selection/<int:selection_id>/edit"),
        edit_selection,
        name=prefix("edit_selection", "-"),
    ),
    path(prefix("references/"), get_references, name=prefix("references", "-")),
    path(
        prefix("user-preferences/<int:user_id>"),
        get_user_preferences,
        name=prefix("get_user_preferences", "-"),
    ),
    path(
        prefix("images/tree/<int:tree_id>/"),
        post_tree_image,
        name=prefix("post_tree_image", "-"),
    ),
    path(prefix("images/<int:id>"), get_tree_image, name=prefix("get_tree_image", "-")),
]
