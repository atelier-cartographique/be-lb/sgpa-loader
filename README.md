# Application de Gestion du Patrimoine Arboré

## Fixtures

Reference data, i.e., the parks with their geometries, the list of tree properties (symptoms, pests and pathogens, tree environment, etc.) can be loaded with these fixtures

```bash
python3 manage.py loaddata parkcolors parks_bxl recommendation_kind  tree_environment  tree_epiphyte  tree_kind  tree_pathogen  tree_pest  tree_status  tree_structure  tree_symptom_leave  tree_symptom_root  tree_symptom_trunk tree_symptom_branch tree_symptom_collar workkind workteam
```

## Commands

### Import geometry parks

Although there is a fixture for the parks, a new set of parks can be added using the command `import_geometry_parks`, based on a geojson file of the parks. Do not run this if you've loaded the fixture `park_bxl`.

```bash
python3 manage.py import_geometry_parks
```

### Import trees

The original trees table can be imported using the command `import_trees`.

To use it, first get a SQL dump of the trees database and import it in the db in a table named as `sgpa_tree_legacy`.

If the imported table is not `sgpa_tree_legacy`, we have to rename it manually the imported table into `sgpa_tree_legacy`.

The model `TreeLegacy` is used to read from this table of imported trees. This model was generated using the `inspectdb` command.

```bash
python3 manage.py import_trees
```

### Seed cached trees

This command makes a read-only copy of the serialized trees in a table based on a serialized model `CachedTree`. This copy is used for accessing the trees in the application for performance reasons. The command must be run after an import of the trees.

```bash
python3 manage.py seed_cached_trees
```


## Tests

```bash
python3 manage.py test sgpa
```
