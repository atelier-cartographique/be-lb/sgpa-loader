# usage: python kind2yaml.py > ../../sgpa/fixtures/tree_kind.yaml

species = set()
hierarchy = dict()


class Hierarchy:

    def __init__(self, hid, name, parent_id = None):
        self.hid = hid
        self.name = name
        self.parent_id = parent_id


with open('./kind.txt') as f:
    for l in f.readlines():
        k = [k.strip().strip("'") for k in l.rstrip().split(' ') if k != '']

        if len(k) == 1:
            species.add((k[0], None, None))
        elif len(k) == 2:
            species.add((k[0], k[1], None))
        elif len(k) == 3 and k[1] == 'x':
            species.add((k[0], k[1] + ' ' + k[2], None))
        elif len(k) >= 3:
            species.add((k[0], k[1], ' '.join(k[2:])))

i = 10001
for sp in species:
    (s, g, t) = sp
    if not (s, None, None) in hierarchy:
        sH = Hierarchy(i, s)
        hierarchy[s, None, None] = sH
        i = i + 1
    else:
        sH = hierarchy[s, None, None]

    if not (s, g, None) in hierarchy:
        gH = Hierarchy(i, g, sH.hid)
        i = i + 1
        hierarchy[s, g, None] = gH
    else:
        gH = hierarchy[(s, g, None)]

    if not (s, g, t) in hierarchy:
        tH = Hierarchy(i, t, gH.hid)
        i = i + 1
        hierarchy[(s, g, t)] = tH

# add in a list for ordering
lHierarchy = list()
for sp in hierarchy.values():
    lHierarchy.append(sp)

print('- model: sgpa.TreeKind')
print('  pk: 10000')
print('  fields:')
print('    label: Indeterminatum')
print('    common_name: {fr: \'Indéterminé\', nl: \'Onbepaald\'}')


for e in sorted(lHierarchy, key=lambda e: e.hid):
    print('- model: sgpa.TreeKind')
    print('  pk: ' + str(e.hid))
    print('  fields:')
    print('    label: ' + e.name)
    print('    common_name: {fr: \'\'}')
    if (e.parent_id is None):
        continue
    print('    parent: ' + str(e.parent_id))
