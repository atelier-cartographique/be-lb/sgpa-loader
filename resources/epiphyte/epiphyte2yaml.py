
i = 10000
with open('./epiphyte.txt') as f:
    for f in f.readlines():
        print('- model: sgpa.TreeEpiphyte')
        print('  pk: ' + str(i))
        print('  fields:')
        print('    label: {fr: ' + f.rstrip() + '}')
        i = i + 1
