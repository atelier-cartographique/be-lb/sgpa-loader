
i = 10000
with open('./pathogens.txt') as f:
    for f in f.readlines():
        print('- model: sgpa.TreePathogen')
        print('  pk: ' + str(i))
        print('  fields:')
        print('    label: {fr: ' + f.rstrip() + '}')
        i = i + 1
